// Query Operators
// Field Projection - exclusion of properties

// [SECTION] Comparison Query Operators

// $gt/$gte operator
	/*
		- allows us to find documents that have field values greater than or equal to a specified value.

		SYNTAX:
			db.collectionName.find({ field: {$gt: value} });
			db.collectionName.find({ field: {$gte: value} });
	*/

	db.users.find({ age: {$gt: 65} }); // will return only 2 records, billgates EXcluded.
	db.users.find({ age: {$gte: 65} }); // will return 3 records including the age equal to 65.

// $lt/$lte operator
	/*
		- allows us to find documents that have field values less than or equal to a specified value.

		SYNTAX:
			db.collectionName.find({ field: {$lt: value} });
			db.collectionName.find({ field: {$lte: value} });
	*/

	db.users.find({ age: {$lt: 65} }); // returns only 1 record
	db.users.find({ age: {$lte: 65} });

// $ne operator
	/*
		- allows us to find documents that have field values not equal to a specified value.

		SYNTAX:
			db.collectionName.find({ field: {$ne: value} });
	*/

	db.users.find({ age: {$ne: 82} }); // only Neil is not returned

// $in operator
	/*
		- allows us to find documents with specific match criteria of one field using different values.

		SYNTAX:
			db.collectionName.find({ field: {$in: {valueA, valueB} } });
	*/

	db.users.find({ lastName: { $in: ["Hawking", "Doe"] } });
	db.users.find({ courses: { $in: ["HTML", "React"] } });


// [SECTION] Logical Query Operators

// $or operator
	/*
		- allows us to find documents that match a single criteria from multiple provided search criteria.

		SYNTAX:
			db.collectionName.find( {$or: [ {fieldA:valueA}, {fieldB:valueB} ]} );
	*/

	db.users.find({
		$or:[
				{ firstName: "Neil"},
				{ age: 25}
			]
	});

	// with comparison query operator
	db.users.find({
		$or:[
				{ firstName: "Neil"},
				{ age: {$gt: 25} }
			]
	});

// $and operator
	/*
		- allows us to find documents that matching multiple criteria in a single field.

		SYNTAX:
			db.collectionName.find( {$and: [ {fieldA:valueA}, {fieldB:valueB} ]} );
	*/

	// with comparison queary
	db.users.find({
		$and:[
				{ age: {$ne: 81} },
				{ age: {$ne: 76} }
			]
	});


// Mini Activity

	// Look for the users that have the courses "Laravel" & "React" and whose age is less than 80 years old.

	// Take a screenshot of result and send it to the batch hangouts.

	// Expected Result: Stephen Hawking and Bill Gates
db.users.find({
	$and:[
		{ courses: { $in: ["Laravel", "React"] } },
		{ age: {$lt: 80} }
	]
});

// End of Mini Activity


// [SECTION] Field Projection
	// - to help with the readability of the values returned, we can include/exclude fields from the retrieve results.

// INCLUSION
	/*
		- allows us to include/add specific fields only when retrieving documents
		- the value provided is 1 to denote that the field is being included.

		SYNTAX:
			db.collectionName.find( {criteria} , {field: 1} );
	*/

	db.users.find(
		{
			firstName: "Jane"
		},
		{
			firstName: 1,
			lastName: 1,
			contact: 1
		}
	);

// EXCLUSION
	/*
		- allows us to exclude/remove specific firlds only when retrieving documents.
		- the value provided is 0 to denote that the field is being excluded.

		SYNTAX:
			db.collectionName.find( {criteria} , {field: 0} );
	*/

	db.users.find(
		{
			firstName: "Jane"
		},
		{
			_id: 0,
			contact: 0,
			department: 0
		}
	);


// Mini Activity
        
	// Using the Field projection, Return the User's firstName, lastName, and contact field where lastName is equal to "Doe".

	// Take a screenshot of result and send it to the batch hangouts.

db.users.find(
	{
		lastName: "Doe"
	},
	{
		_id: 0,
		age: 0,
		courses: 0,
		department: 0
	}
);
// End of Mini Activity

db.users.find(
	{
		lastName: "Doe"
	},
	{
		firstName: 1,
		lastName: 1,
		contact: 1,
		_id: 0
	}
); //same result

	// Suppressing the ID Field
	/*	
		- when using field projection, field inclusion and exclusion may not be used at the same time.
		- exclusing "_id" field is the only exception to this rule.
		
		SYNTAX:
			db.collectionName.find( {criteria} , {field: 1, _id: 0} );
	*/

		db.users.find(
			{
				lastName: "Doe"
			},
			{
				firstName: 1,
				lastName: 1,
				contact: 1,
				_id: 0,
				courses: 0
			}
		); // results to error, does not accept multiple field projection, "_id" is the only exeption since it is, by default, INcluded/

// Returning a Specific field in Embedded Documents.
	// include 
	db.users.find(
		{
			firstName: "Jane"
		},
		{
			firstName: 1,
			lastName: 1,
			"contact.phone": 1
		}
	);

	// exclude
	db.users.find(
		{
			firstName: "Jane"
		},
		{
			"contact.phone": 0
		}
	);

// Projects specific elements in the returned array.
/*
	-the $slice operator allows to retrieve element that matches the criteria

	- shows the elements array based on sliced count
	SYNTAX:
		db.collectionName.find({criteria}, arrayField: {$slice: count});
	
	- shows the elements array based on sliced count from index
	SYNTAX:
		db.collectionName.find({criteria}, arrayField: {$slice: [index, count] });


*/
	db.users.find(
		{
			firstName: "Jane"
		},
		{
			courses: {$slice: 1}
			// courses: {$slice: 4} // if excess, returns all array
		}
	);

	db.users.find(
		{
			firstName: "Jane"
		},
		{
			courses: { $slice: [1, 2] }
		}
	);

// [SECTION] Evaluation Query Operator

	db.users.find( { firstName: "jane"});

	// $regex operator
	/*
		- allows us to find documents that match a specific string pattern using regular expression/"regex".

		SYNTAX:
			db.collectionName.find({field: {$regex: "pattern", $options: "optionValue"})
	*/

		// case sensitive query
		db.users.find( {firstName: {$regex: "J"} }); // returns Jane
		db.users.find( {firstName: {$regex: "N"} }); // returns Neil
		db.users.find( {firstName: {$regex: "e"} }); // returns all with small letter e; Jane, Neil, Stephen

		// case insensitive query
		db.users.find( {firstName: {$regex: "ne", $options: "$i"} }); //returns Jane and Neil